const express = require('express');
const app = express();
const soap = require('soap');
const myService = {
    MyService: {
        MyPort: {
            GetCourierName: function (args) {
                console.log("wywołanie GetCourierName")
                const companyname1 = "DPD"
                const companyname1_cost = 23
                const companyname2 = "UPS"
                const companyname2_cost = 25
                return [{
                    "CompanyName": companyname1,
                    "Cost": companyname1_cost
                }, {
                    "CompanyName": companyname2,
                    "Cost": companyname2_cost

                }];
            },
            MakeOrder: function (args) {
                console.log("wywołanie MakeOrder")
                let now = new Date();
                const code = now.getFullYear() + "" + (now.getMonth() + 1) + now.getDate() + 'w' + Math.trunc((Math.random() + 1) * 1000);
                return [{
                    "PackageCode": code

                }];
            },
            CheckStatus: function (args) {
                console.log("wywołanie CheckStatus")
                const randimize = Math.random() * 10
                var status = ""
                if (randimize > 5) {
                    status = "odebrana, paczka jest oddziale"
                } else {
                    status = "paczka jeszcze nie odebrana"
                }
                return [{
                    "PackageStatus": status
                }];
            }
        }
    }
};

var path = __dirname + '/index.html'
const xml = require('fs').readFileSync('./myservice.wsdl', 'utf8');
app.use(express.static(__dirname));
app.get('/', (req, res) => {
    res.sendFile(path)
});
app.listen(3000, function () {
    //and all other routes & middleware will continue to work
    soap.listen(app, '/wsdl', myService, xml, function () {
        console.log('server initialized');
    });
});
